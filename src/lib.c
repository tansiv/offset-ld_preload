#define _GNU_SOURCE
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/times.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <sys/mman.h>
#include <unistd.h>
#include <dlfcn.h>
#include <stdbool.h>
#include <sys/socket.h>

#define ONE_SECOND_NS 1000000000 // 10⁹

#define INIT_LOAD_FUN(pointer, symbol) \
	pointer = dlsym(RTLD_NEXT, symbol); \
	if (pointer==NULL) { \
		fprintf(stderr, "TANSIV LD_PRELOAD failed to get %s symbol: %s\n", symbol, dlerror()); \
		exit(-2); \
	}

static const char *sharedfilepath="/dev/shm/tansiv-time";

static struct timespec *offset=NULL;

static int (*_realClockGettime)(clockid_t, struct timespec *) = NULL;
static int (*_realGettimeofday)(struct timeval *, struct timezone *__restrict) = NULL;
static clock_t (*_realTimes)(struct tms *buf) = NULL;
static int (*_realClockNanosleep)(clockid_t clock_id, int flags, const struct timespec *req, struct timespec *rem) = NULL;
static ssize_t (*_realRecv)(int sockfd, void *buf, size_t len, int flags);
static ssize_t (*_realRecvfrom)(int sockfd, void *buf, size_t len, int flags, struct sockaddr *src_addr, socklen_t *addrlen);
static ssize_t (*_realRecvmsg)(int sockfd, struct msghdr *msg, int flags);

static clock_t _clk_tck = -1;

__attribute__((constructor)) void init(void) {
	int fd = open(sharedfilepath, O_RDONLY);
	if (fd==-1) {
		perror("TANSIV LD_PRELOAD failed to open shared file");
		exit(-2);
	}
	offset=mmap(NULL, sizeof(struct timespec), PROT_READ, MAP_SHARED, fd, 0);
	if (offset==(void*)-1) {
		perror("TANSIV LD_PRELOAD failed to mmap shared file");
		exit(-2);
	}
	if (close(fd)!=0) {
		perror("TANSIV LD_PRELOAD failed to close shared file descriptor");
		exit(-2);
	}
	// note that mmaps are automatically unmapped when the process is terminated

	INIT_LOAD_FUN(_realClockGettime, "clock_gettime");
	INIT_LOAD_FUN(_realGettimeofday, "gettimeofday");
	INIT_LOAD_FUN(_realTimes, "times");
	INIT_LOAD_FUN(_realClockNanosleep, "clock_nanosleep");
	INIT_LOAD_FUN(_realRecv, "recv");
	INIT_LOAD_FUN(_realRecvfrom, "recvfrom");
	INIT_LOAD_FUN(_realRecvmsg, "recvmsg");

	_clk_tck = sysconf(_SC_CLK_TCK); // can be determined on a given system using `getconf CLK_TCK` in a shell
	if (_clk_tck<=0) {
		perror("TANSIV LD_PRELOAD failed to determine CLK_TCK configuration value");
		exit(-2);
	}
	if (_clk_tck>ONE_SECOND_NS) {
		fprintf(stderr, "TANSIV LD_PRELOAD didn’t expect CLK_TCK to have sub-nanosecond precision, it is %ld\n", _clk_tck);
	}
}

static void _sum_timespecs(struct timespec *out, const struct timespec *left, const struct timespec *right)
{
	out->tv_sec=left->tv_sec+right->tv_sec;
	out->tv_nsec=left->tv_nsec+right->tv_nsec;
	if (out->tv_nsec>=ONE_SECOND_NS) {
		out->tv_nsec-=ONE_SECOND_NS;
		out->tv_sec+=1;
	}
}
static void _sum_with_timespec(struct timespec *out, const struct timespec *right)
{
	out->tv_sec+=right->tv_sec;
	out->tv_nsec+=right->tv_nsec;
	if (out->tv_nsec>=ONE_SECOND_NS) {
		out->tv_nsec-=ONE_SECOND_NS;
		out->tv_sec+=1;
	}
}

static bool _is_offsetted_clock(clockid_t clk_id)
{
	// In particular, this excludes CLOCK_*_CPUTIME_ID and clocks returned by clock_getcpuclockid and pthread_getcpuclockid
	return \
		clk_id==CLOCK_REALTIME ||
		clk_id==CLOCK_REALTIME_COARSE || // Linux-specific
		clk_id==CLOCK_MONOTONIC ||
		clk_id==CLOCK_MONOTONIC_COARSE || // Linux-specific
		clk_id==CLOCK_MONOTONIC_RAW || // Linux-specific
		clk_id==CLOCK_BOOTTIME; // Linux-specific
}
static bool _is_sleep_offsetted_clock(clockid_t clk_id)
{
	// *_COARSE and *_RAW can’t be used to sleep, apparently
	return \
		clk_id==CLOCK_REALTIME ||
		clk_id==CLOCK_MONOTONIC ||
		clk_id==CLOCK_BOOTTIME; // Linux-specific
}

int clock_gettime(clockid_t clk_id, struct timespec *tp)
{
	int res = _realClockGettime(clk_id, tp);
	if (res==0 && _is_offsetted_clock(clk_id)) {
		tp->tv_sec-=offset->tv_sec;
		tp->tv_nsec-=offset->tv_nsec;
		// this assumes tv_nsec is between 0 (included) and 1 second (in nanoseconds, excluded)
		// and ensures it is still the case afterwards
		if (tp->tv_nsec<0) {
			tp->tv_nsec+=ONE_SECOND_NS;
			tp->tv_sec-=1;
		}
	}
	return res;
}

time_t time(time_t *tloc)
{
	struct timespec tp;
	if (clock_gettime(CLOCK_REALTIME, &tp)==0) { // our modified verison
		if (tloc!=NULL) // though this use is considered obsolescent
			*tloc=tp.tv_sec;
		return tp.tv_sec;
	} else return ((time_t) -1);
}

int gettimeofday(struct timeval *__restrict tv, void *__restrict tz)
{
	int res = _realGettimeofday(tv, tz);
	if (res==0) {
		tv->tv_sec-=offset->tv_sec;
		tv->tv_usec-=offset->tv_nsec/1000;
		if (tv->tv_usec<0) {
			tv->tv_usec+=1000000;
			tv->tv_sec-=1;
		}
	}
	return res;
}

clock_t times(struct tms *buf)
{
	// the man page recommends against using this function’s return value
	// but it is included for completeness
	clock_t res = _realTimes(buf);
	if (res!=-1) {
		// I expect CLK_TCK to have much less precision than nanoseconds
		// On my machine, its value is 100, which is much smaller than 10⁹
		res-= \
			offset->tv_sec*_clk_tck + \
			offset->tv_nsec/(ONE_SECOND_NS/_clk_tck);
	}
	return res;
}

int clock_nanosleep(clockid_t clock_id, int flags, const struct timespec *request, struct timespec *rem)
{
	if (!_is_sleep_offsetted_clock(clock_id))
		return _realClockNanosleep(clock_id, flags, request, rem);
	// we will assume that the offset can only move forward
	struct timespec initial_offset=*offset;
	struct timespec altreq;
	const struct timespec *req=request;
	struct timespec offsetted_req;
	if (flags!=TIMER_ABSTIME) {
		if (clock_gettime(clock_id, &altreq)!=0) // fake time
			return errno;
		_sum_with_timespec(&altreq, request); // + relative sleep request
		req=&altreq;
	}
	int res;
	struct timespec next_offset=initial_offset;
	do {
		_sum_timespecs(&offsetted_req, req, &next_offset);
		initial_offset=next_offset;
		res = _realClockNanosleep(clock_id, TIMER_ABSTIME, &offsetted_req, rem);
		next_offset=*offset;
	} while (
		res==0 &&
		(initial_offset.tv_nsec!=next_offset.tv_nsec || initial_offset.tv_sec!=next_offset.tv_sec)
	);
	if (res==EINTR && rem!=NULL && flags!=TIMER_ABSTIME) {
		struct timespec cur_fake_time;
		if (clock_gettime(clock_id, &cur_fake_time)!=0)
			return errno;
		rem->tv_sec=altreq.tv_sec-cur_fake_time.tv_sec;
		rem->tv_nsec=altreq.tv_nsec-cur_fake_time.tv_nsec;
		if (rem->tv_nsec<0) {
			rem->tv_nsec+=ONE_SECOND_NS;
			rem->tv_sec-=1;
		}
	}
	return res;
}

int nanosleep(const struct timespec *req, struct timespec *rem)
{
	int res = clock_nanosleep(CLOCK_REALTIME, 0, req, rem); // our modified version
	if (res!=0) {
		errno=res;
		return -1;
	}
	return 0;
}

unsigned int sleep(unsigned int seconds)
{
	struct timespec req = {seconds, 0};
	struct timespec rem;
	int res = nanosleep(&req, &rem); // our modified version
	if (res==0)
		return 0;
	else
		return rem.tv_sec;
}

int usleep(useconds_t usec)
{
	struct timespec req = {usec/1000000, 1000*(usec%1000000)};
	return nanosleep(&req, NULL); // our modified version
}


// TODO: for now this does not consider timeout, which is the only case where this wrapper will be useful (see TODO.md)
ssize_t recv(int sockfd, void *buf, size_t len, int flags)
{

	ssize_t ret;
	do {
		ret = _realRecv(sockfd, buf, len, flags);
	} while (ret==-1 && errno==EINTR);
	return ret;
}
ssize_t recvfrom(int sockfd, void *buf, size_t len, int flags, struct sockaddr *src_addr, socklen_t *addrlen)
{
	ssize_t ret;
	do {
		ret = _realRecvfrom(sockfd, buf, len, flags, src_addr, addrlen);
	} while (ret==-1 && errno==EINTR);
	return ret;
}
ssize_t recvmsg(int sockfd, struct msghdr *msg, int flags)
{
	ssize_t ret;
	do {
		ret = _realRecvmsg(sockfd, msg, flags);
	} while (ret==-1 && errno==EINTR);
	return ret;
}
