 - Clean up
   - [ ] additions and substractions of timespecs are done often and require many lines of code
     They could be implemented in static functions (both for substracting a timespec from an existing one, and storing the substraction in another timespec).
     This has already been done for one of the four cases.
 - [x] Unit testing: tests are currently manual and not very simple: we need tests to verify all uses of all functions more easily, for example, for the `clock_nanosleep` absolute mode
 - [ ] file timestamps currently use the real clock, but they can be modified
   libarchive does this when extracting archives: https://github.com/libarchive/libarchive/blob/master/libarchive/archive_write_disk_posix.c#L3487
 - [ ] Investigate [faketime](https://github.com/wolfcw/libfaketime) and its list of wrapped functions
 - [x] Handle `clock_getcpuclockid` and `pthread_getcpuclockid` clocks correctly in `clock_nanosleep` and maybe `clock_gettime`
   These are “CPU-time” clocks, which means they don’t need to be changed, but they may currently be.
 - [x] Fix the relative clock_nanosleep implementation : it doesn’t currently sleep the correct time when stopping the program for more time than the sleeping takes
   - One option would be to always use relative mode, if it works
 - [ ] A quick test using select as a sleep function (see manual) shows that it doesn’t behave in the same way as nanosleep, etc. when receiving a SIGSTOP
   The timeout can extend beyond the continuation, even if it was stopped for more time.
    - This seems to indicate that it doesn’t need to be wrapped
    - [x] Test nanosleep and select behaviors when using cgroup.freeze
      - They behave the same as with SIGSTOPs
 - [ ] Implement or lie about kernel support for the SIOCGSTAMP ioctl and SO_TIMESTAMP control message (see socket(7))
 - [ ] It is possible to set timeouts on sockets (see socket(7))
    - Notably, `recvmsg` (and similar) will automatically restart when no timeout is set, but will return `-EINTR` if it is interrupted by a signal (**or a container freeze**) and a timeout exists
      - A possibility would be to store the time these functions are run, calculate the absolute time of timeout expiration, and run the functions again on timeout expiration or EINTR (assuming a freeze was responsible) with a timeout determined by ({absolute time of timeout expiration} - {current time})
        - An option to check if a freeze was responsible for an EINTR is to check whether the offset changed: this can only be wrong if a signal arrives right before a freeze, and the freeze happens before copying the offset for the `!=` test
          - Making a copy and comparing the copy should help reduce the occurence of this race condition
      - As these functions don’t allow specifying a timeout, it is necessary to change socket options in order to run with a different option, which could affect other areas of the program if it has multiple threads (but is unlikely)
