#define _POSIX_C_SOURCE 199309L
#include <sys/types.h>
#include <time.h>
#include <stdbool.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdlib.h>

#include "stopcomm.h"

#define ONE_SECOND_NS 1000000000 // 10⁹

static const char null_byte = '\0';

static pid_t *processes = NULL;
static size_t process_number = 0;

static struct timespec prev_stop;

static const char *offset_path = "/dev/shm/tansiv-time";
static struct timespec *offset = NULL;

void stopper_set_processes(size_t pnum, pid_t pids[])
{
	processes=pids;
	process_number=pnum;
}

static void normalize_offset()
{
	if (offset->tv_nsec>=ONE_SECOND_NS) {
		offset->tv_sec+=1;
		offset->tv_nsec-=ONE_SECOND_NS;
	} else if (offset->tv_nsec<0) {
		offset->tv_sec-=1;
		offset->tv_nsec+=ONE_SECOND_NS;
	}
}

static bool killall(int sig)
{
	bool ret=true;
	for (size_t i=0; i<process_number; ++i) {
		if (kill(processes[i], sig)!=0) {
			perror("Failed to send signal");
			ret=false;
		}
	}
	return ret;
}

#ifndef DUMMY_MODE
static bool didstop = false;
#endif

static bool stop_processes()
{
#ifndef DUMMY_MODE
	if (didstop)
		return true;
	if (killall(SIGSTOP)) {
		if (clock_gettime(CLOCK_MONOTONIC, &prev_stop)!=0) {
			perror("Failed to stop processes");
			return false;
		}
		didstop=true;
		return true;
	} else {
		return false;
	}
#else
	return true;
#endif
}

static bool cont_processes()
{
#ifndef DUMMY_MODE
	if (!didstop)
		return true;
	struct timespec cur_time;
	if (clock_gettime(CLOCK_MONOTONIC, &cur_time)!=0) {
		perror("Failed to continue processes");
		return false;
	}
	offset->tv_sec+=cur_time.tv_sec-prev_stop.tv_sec;
	offset->tv_nsec+=cur_time.tv_nsec-prev_stop.tv_nsec;
	normalize_offset();
	if (!killall(SIGCONT)) {
		return false;
	}
	didstop=false;
#endif
	return true;
}

static bool process_requests(size_t request_number, struct stopcomm_request *requests)
{
	for (size_t i=0; i<request_number; ++i) {
		if (requests[i].wait_before.tv_sec!=0 || requests[i].wait_before.tv_nsec!=0) {
			if (nanosleep(&requests[i].wait_before, NULL)!=0) {
				perror("Error while sleeping before request");
				return false;
			}
		}
#ifndef DUMMY_MODE
		stop_processes();
		if (nanosleep(&requests[i].stop_time, NULL)!=0) {
			perror("Error while sleeping during request");
			return false;
		}
		cont_processes();
#endif
	}
	return true;
}

static void remove_shared_file()
{
	if (unlink(offset_path)!=0)
		perror("Failed to remove shared offset file");
}

bool stopper_init()
{
	int fd = open(offset_path, O_CREAT|O_RDWR);
	if (fd==-1) {
		perror("Couldn’t open offset shared file");
		return false;
	}
	atexit(remove_shared_file);
	if (ftruncate(fd, sizeof(struct timespec))!=0) {
		perror("Couldn’t truncate offset shared file");
		return false;
	}
	offset = mmap(NULL, sizeof(struct timespec), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0); // will be automatically munmap-ed when the process terminates (see the manual page)
	if (offset==MAP_FAILED) {
		perror("Couldn’t mmap offset shared file");
		if (close(fd)!=0) {
			perror("Additionnally, couldn’t close offset shared file");
		}
		return false;
	}
	if (close(fd)!=0) {
		perror("Couldn’t close offset shared file");
		return false;
	}
	return true;
}

bool stopper_run(int fd)
{
	while (true) {
		size_t request_number;
		ssize_t readret;
		readret = read(fd, &request_number, sizeof(size_t));
		if (readret!=sizeof(size_t)) {
			perror("Failed to read request");
			return false;
		}
		if (request_number==0)
			return true;
		struct stopcomm_request *requests = calloc(request_number, sizeof(struct stopcomm_request));
		if (requests==NULL) {
			perror("Failed to allocate requests buffer");
			return false;
		}
		readret = read(fd, requests, request_number*sizeof(struct stopcomm_request));
		if (readret!=request_number*sizeof(struct stopcomm_request)) {
			free(requests);
			perror("Failed to read requests");
			return false;
		}

		if (requests[0].wait_before.tv_sec==0 && requests[0].wait_before.tv_nsec==0) {
			stop_processes();
			if (write(fd, &null_byte, 1)!=1) {
				perror("Failed to inform process");
				free(requests);
				return false;
			}
		}
		bool processing=process_requests(request_number, requests);
		free(requests);
		if (!processing)
			return false;
	}
}
