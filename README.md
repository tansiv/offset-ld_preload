# TANSIV (container) LD_PRELOAD time shim

This allows for programs to run as if a specified and modifiable time didn’t pass.

This works by `mmap`ing `/dev/shm/tansiv-time` (read-only), and interpreting its contents as a `struct timespec`, which can be updated from another process.
This `struct timespec`’s value is substracted from all C library functions that read the current time, such as `time` and `clock_gettime`.


## Currently supported functions

 - Time reading
   - `clock_gettime`
   - `time`
   - `gettimeofday`
   - `times`
 - Sleeping
   - `clock_nanosleep`
   - `nanosleep`
   - `sleep`
   - `usleep`

## Currently unsupported functions

 - Time reading
   - `ftime` (deprecated)
   - `timespec_get` (C11)
 - Alarms and timers
 - Network features
   - `SIOCGSTAMP`
   - `SO_TIMESTAMP`
   - `SO_RCVTIMEO` and `SO_SNDTIMEO`


## Acknowledgements

This code was inspired by [this ./play.it hack](https://forge.dotslashplay.it/vv221/games/-/blob/main/games/play-anomaly-2.sh#L153), licensed under BSD-2.
