#include "stopcomm.h"
#include <time.h>
#include <stdio.h>
#include <stdbool.h>

bool run_test()
{
	time_t start=time(NULL);
	struct stopcomm_request request = {
		{0,  0},
		{10, 0}
	};
	stopcomm_requests(1, &request);
	time_t end=time(NULL);
	if (end-start>1) {
		printf("time() test FAILED.\n");
		return false;
	}
	return true;
}
