#include "stopcomm.h"
#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>

bool run_test()
{
	struct stopcomm_request requests[] = {
		{
			{0,  500000000},
			{7, 0}
		},
		{
			{1, 0},
			{2, 500000000}
		}
	};
	stopcomm_requests(2, requests);
	time_t start=time(NULL);
	if (sleep(2)!=0) {
		perror("sleep didn’t sleep for the requested time");
		return false;
	}
	time_t end=time(NULL);
	if (end-start<2 || end-start>3) {
		printf("sleep() (multistop) test FAILED: time() measures a run time of %ld seconds\n", end-start);
		return false;
	}
	return true;
}
