#include "stopcomm.h"
#include <time.h>
#include <sys/times.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>

bool run_test()
{

	long clk_tck = sysconf(_SC_CLK_TCK);
	struct tms start;
	clock_t startret=times(&start);
	if (startret==-1) {
		perror("times (start)");
		return false;
	}
	struct stopcomm_request request = {
		{0,  0},
		{10, 0}
	};
	stopcomm_requests(1, &request);
	struct tms end;
	clock_t endret=times(&end);
	if (endret==-1) {
		perror("times (end)");
		return false;
	}
	clock_t threshold = clk_tck/1000;
	if (threshold<1) // it most likely will be
		threshold=1;
	if (endret-startret>threshold || \
	    end.tms_utime-start.tms_utime>threshold || \
	    end.tms_stime-start.tms_stime>threshold || \
	    end.tms_cutime-start.tms_cutime>threshold || \
	    end.tms_cstime-start.tms_cstime>threshold) {
		printf("times() test FAILED.\n");
		return false;
	}

	return true;
}
