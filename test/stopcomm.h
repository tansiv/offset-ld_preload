#ifndef STOPCOMM_H
#define STOPCOMM_H

#include <time.h>

struct stopcomm_request {
	struct timespec wait_before;
	struct timespec stop_time;
};

void stopcomm_requests(size_t request_number, struct stopcomm_request *requests);

#endif // STOPCOMM_H
