#include "stopcomm.h"
#include <time.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdbool.h>

bool run_test()
{
	struct timeval start;
	if (gettimeofday(&start, NULL)!=0) {
		perror("gettimeofday (start)");
		return false;
	}
	struct stopcomm_request request = {
		{0,  0},
		{10, 0}
	};
	stopcomm_requests(1, &request);
	struct timeval end;
	if (gettimeofday(&end, NULL)!=0) {
		perror("gettimeofday (end)");
		return false;
	}
	if ((end.tv_sec-start.tv_sec)*1000000+(end.tv_usec-start.tv_usec)>1000) {
		printf("gettimeofday() test FAILED.\n");
		return false;
	}
	return true;
}
