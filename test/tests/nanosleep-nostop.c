#include "stopcomm.h"
#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>

bool run_test()
{
	struct timespec start;
	if (clock_gettime(CLOCK_REALTIME, &start)!=0) {
		perror("clock_gettime (start)");
		return false;
	}
	struct timespec request = {
		2,
		500000000
	};
	if (nanosleep(&request, NULL)!=0) {
		perror("nanosleep didn’t sleep for the requested time");
		return false;
	}
	struct timespec end;
	if (clock_gettime(CLOCK_REALTIME, &end)!=0) {
		perror("clock_gettime (end)");
		return false;
	}
	if (end.tv_sec-start.tv_sec<2 || end.tv_sec-start.tv_sec>3 ||
	    (end.tv_sec-start.tv_sec)*1000000000+(end.tv_nsec-start.tv_nsec)<2500000000 ||
	    (end.tv_sec-start.tv_sec)*1000000000+(end.tv_nsec-start.tv_nsec)>2501000000
	) {
		printf("nanosleep() (no stop) test FAILED: clock_gettime(REALTIME) measures a run time of %ld seconds and %ld nanoseconds\n", end.tv_sec-start.tv_sec, end.tv_nsec-start.tv_nsec);
		return false;
	}
	return true;
}
