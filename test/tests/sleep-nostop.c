#include "stopcomm.h"
#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>

bool run_test()
{
	time_t start=time(NULL);
	if (sleep(2)!=0) {
		perror("sleep didn’t sleep for the requested time");
		return false;
	}
	time_t end=time(NULL);
	if (end-start<2 || end-start>3) {
		printf("sleep() (no stop) test FAILED: time() measures a run time of %ld seconds\n", end-start);
		return false;
	}
	return true;
}
