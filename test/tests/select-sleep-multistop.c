#include "stopcomm.h"
#include <time.h>
#include <stdio.h>
#include <sys/time.h>
#include <sys/select.h>
#include <stdbool.h>

bool select_sleep(struct timeval timeout)
{
	fd_set readfds, writefds, exceptfds;
	FD_ZERO(&readfds);
	FD_ZERO(&writefds);
	FD_ZERO(&exceptfds);
	// timeout should be copied to the stack, and therefore not modified in the calling function
	return select(0, &readfds, &writefds, &exceptfds, &timeout)==0;
}

bool run_test()
{
	struct stopcomm_request requests[] = {
		{
			{0,  500000000},
			{7, 0}
		},
		{
			{1, 0},
			{2, 500000000}
		}
	};
	stopcomm_requests(2, requests);
	struct timeval start;
	if (gettimeofday(&start, NULL)!=0) {
		perror("gettimeofday (start)");
		return false;
	}
	struct timeval request = {
		2,
		500000
	};
	if (!select_sleep(request)) {
		perror("select()");
		return false;
	}
	struct timeval end;
	if (gettimeofday(&end, NULL)!=0) {
		perror("gettimeofday (end)");
		return false;
	}
	if (end.tv_sec-start.tv_sec<2 || end.tv_sec-start.tv_sec>3 ||
	    (end.tv_sec-start.tv_sec)*1000000+(end.tv_usec-start.tv_usec)<2500000 ||
	    (end.tv_sec-start.tv_sec)*1000000+(end.tv_usec-start.tv_usec)>2505000
	) {
		printf("select() sleep (multi stop) test FAILED: gettimeofday() measures a run time of %ld seconds and %ld microseconds\n", end.tv_sec-start.tv_sec, end.tv_usec-start.tv_usec);
		return false;
	}
	return true;
}
