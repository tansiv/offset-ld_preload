#include "stopcomm.h"
#include <time.h>
#include <stdio.h>
#include <stdbool.h>

bool run_test()
{

	clockid_t clocks[] = {
		CLOCK_REALTIME,
		CLOCK_REALTIME_COARSE,
		CLOCK_MONOTONIC,
		CLOCK_MONOTONIC_COARSE,
		CLOCK_MONOTONIC_RAW,
		CLOCK_BOOTTIME,
		CLOCK_PROCESS_CPUTIME_ID,
		CLOCK_THREAD_CPUTIME_ID
	};
	size_t clock_num = sizeof(clocks)/sizeof(clockid_t);

	for (size_t i = 0; i<clock_num; ++i) {
		long threshold=1000000; // 1ms
		if (clocks[i]==CLOCK_REALTIME_COARSE || clocks[i]==CLOCK_MONOTONIC_COARSE) {
			threshold=25000000; // 25ms for coarse clocks
		}
		struct timespec start;
		if (clock_gettime(clocks[i], &start)!=0) {
			perror("clock_gettime (start)");
			return false;
		}
		struct stopcomm_request request = {
			{0,  0},
			{10, 0}
		};
		stopcomm_requests(1, &request);
		struct timespec end;
		if (clock_gettime(clocks[i], &end)!=0) {
			perror("clock_gettime (end)");
			return false;
		}
		if ((end.tv_sec-start.tv_sec)*1000000000+(end.tv_nsec-start.tv_nsec)>1000000) {
			printf("clock_gettime() test FAILED on clock number %lu.\n", i);
			return false;
		}
	}

	return true;
}
