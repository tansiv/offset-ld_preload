#include "stopcomm.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>

extern bool run_test();

static int fd=-1;

static void stopcomm_end()
{
	stopcomm_requests(0, NULL);
	close(fd);
}

int main(int argc, char *argv[])
{
	if (argc!=2 || strlen(argv[1])>sizeof(int)) {
		fprintf(stderr, "This test is meant to be called by the dedicated runner program.\n");
		exit(-1);
	}
	char buf[sizeof(int)];
	strncpy(buf, argv[1], sizeof(int));
	fd = *((int*)buf);
	atexit(stopcomm_end);
	return run_test()? EXIT_SUCCESS : EXIT_FAILURE;
}

void stopcomm_requests(size_t request_number, struct stopcomm_request *requests)
{
	if (write(fd, &request_number, sizeof(size_t))!=sizeof(size_t)) {
		perror("Failed to write requests");
		exit(-1);
	}
	if (request_number>0) {
		size_t request_array_size = sizeof(struct stopcomm_request)*request_number; // could technically overflow
		if (write(fd, requests, request_array_size)!=request_array_size) {
			perror("Failed to write requests");
			exit(-1);
		}

		if (requests[0].wait_before.tv_sec==0 && requests[0].wait_before.tv_nsec==0) {
			char a;
			if (read(fd, &a, 1)!=1) {
				perror("Failed to wait for stopper process");
				exit(-1);
			}
		}
	}
}
