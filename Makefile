.PHONY: clean test

lib.so: src/lib.c
	$(CC) $(CFLAGS) -shared $^ -o $@ -Wall -fPIC -ldl
clean:
	rm --force lib.so; $(MAKE) -C test clean
test: lib.so
	$(MAKE) -C test
