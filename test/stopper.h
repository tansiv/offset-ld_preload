#ifndef STOPPER_H
#define STOPPER_H

#include <sys/types.h>
#include <stdbool.h>

void stopper_set_processes(size_t pnum, pid_t pids[]);
bool stopper_init();
bool stopper_run(int fd);

#endif // STOPPER_H
