#include "stopcomm.h"
#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>

bool run_test()
{

	clockid_t clocks[] = {
		CLOCK_REALTIME,
		CLOCK_MONOTONIC,
		CLOCK_BOOTTIME
	};
	size_t clock_num = sizeof(clocks)/sizeof(clockid_t);

	for (size_t i = 0; i<clock_num; ++i) {
		struct stopcomm_request stoprequests[] = {
			{
				{0, 500000000},
				{7, 0}
			},
			{
				{1, 0},
				{2, 500000000}
			}
		};
		stopcomm_requests(2, stoprequests);
		struct timespec request;
		if (clock_gettime(clocks[i], &request)!=0) {
			perror("clock_gettime (request)");
			return false;
		}
		request.tv_nsec=0;
		request.tv_sec+=3;
		if (clock_nanosleep(clocks[i], TIMER_ABSTIME, &request, NULL)!=0) {
			perror("nanosleep didn’t sleep for the requested time");
			return false;
		}
		struct timespec end;
		if (clock_gettime(clocks[i], &end)!=0) {
			perror("clock_gettime (check)");
			return false;
		}
		if (
			end.tv_sec-request.tv_sec<0 || (end.tv_sec==request.tv_sec && end.tv_nsec-request.tv_nsec<0) ||
			end.tv_sec-request.tv_sec>1 || (end.tv_sec-request.tv_sec)*1000000000+(end.tv_nsec-request.tv_nsec)>1000000 // 1ms margin
		) {
			printf("clock_nanosleep() (no stop) test FAILED on clock number %lu: clock_gettime() measures an end time of %ld seconds and %ld nanoseconds instead of close to %ld seconds and %ld nanoseconds.\n", i, end.tv_sec, end.tv_nsec, request.tv_sec, request.tv_nsec);
			return false;
		}
	}

	return true;
}
