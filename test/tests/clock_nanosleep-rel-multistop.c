#include "stopcomm.h"
#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>

bool run_test()
{

	clockid_t clocks[] = {
		CLOCK_REALTIME,
		CLOCK_MONOTONIC,
		CLOCK_BOOTTIME
	};
	size_t clock_num = sizeof(clocks)/sizeof(clockid_t);

	for (size_t i = 0; i<clock_num; ++i) {
		struct stopcomm_request requests[] = {
			{
				{0, 500000000},
				{7, 0}
			},
			{
				{1, 0},
				{2, 500000000}
			}
		};
		stopcomm_requests(1, requests);
		struct timespec start;
		if (clock_gettime(clocks[i], &start)!=0) {
			perror("clock_gettime (start)");
			return false;
		}
		struct timespec request = {
			2,
			500000000
		};
		if (clock_nanosleep(clocks[i], 0, &request, NULL)!=0) {
			perror("nanosleep didn’t sleep for the requested time");
			return false;
		}
		struct timespec end;
		if (clock_gettime(clocks[i], &end)!=0) {
			perror("clock_gettime (end)");
			return false;
		}
		if (end.tv_sec-start.tv_sec<2 || end.tv_sec-start.tv_sec>3 ||
		    (end.tv_sec-start.tv_sec)*1000000000+(end.tv_nsec-start.tv_nsec)<2500000000 ||
		    (end.tv_sec-start.tv_sec)*1000000000+(end.tv_nsec-start.tv_nsec)>2501000000
		) {
			printf("clock_nanosleep() (single stop) test FAILED on clock number %lu: clock_gettime() measures a run time of %ld seconds and %ld nanoseconds\n", i, end.tv_sec-start.tv_sec, end.tv_nsec-start.tv_nsec);
			return false;
		}
	}

	return true;
}
