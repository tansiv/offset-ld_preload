#define _GNU_SOURCE
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include "stopper.h"

static char *ld_preload = NULL;
static const char *ld_preload_pre="LD_PRELOAD=";
static const char *ld_preload_post="/../lib.so";
void free_ld_preload()
{
	return free(ld_preload);
}

int main(int argc, char *argv[])
{
	char *exec_env[2]={};
	if (argc==0 || strstr(argv[0], "nopreload")==NULL) {
		char *cwd = get_current_dir_name();
		if (cwd==NULL) {
			perror("Couldn’t get current directory");
			return 1;
		}
		size_t ld_preload_size = strlen(ld_preload_pre)+strlen(cwd)+strlen(ld_preload_post)+1;
		ld_preload = malloc(ld_preload_size);
		if (ld_preload==NULL) {
			free(cwd);
			perror("Couldn’t allocate memory");
			return 1;
		}
		atexit(free_ld_preload);
		strcpy(ld_preload, ld_preload_pre);
		strcat(ld_preload, cwd);
		strcat(ld_preload, ld_preload_post);
		exec_env[0]=ld_preload;
	}
	if (!stopper_init())
		return 2;
	for (int i=1; i<argc; ++i) {
		int sv[2];
		if (socketpair(AF_UNIX, SOCK_STREAM, 0, sv)!=0) {
			perror("Couldn’t create socket pair");
			return 1;
		}
		pid_t child_pid = fork();
		if (child_pid==-1) {
			perror("Fork failed");
			return 1;
		}
		if (child_pid==0) {
			// this is the child process
			close(sv[0]);
			char arg1[sizeof(int)+1];
			arg1[sizeof(int)]='\0';
			*((int*)arg1)=sv[1];
			if (execle(argv[i], argv[i], arg1, (char*)NULL, exec_env)==-1) {
				perror("Failed to execute test");
				return 1;
			}
		} else {
			// this is the parent process
			close(sv[1]);
			stopper_set_processes(1, &child_pid);
			if (!stopper_run(sv[0]))
				return 3;
			close(sv[0]);
			int wstatus;
			pid_t ret;
			while ((ret=waitpid(child_pid, &wstatus, 0))!=-1 && \
			       !WIFEXITED(wstatus));
			if (ret==-1) {
				perror("waitpid failed");
				return 1;
			}
			if (WEXITSTATUS(wstatus)!=0)
				return 4;
		}
	}
	return 0;
}
