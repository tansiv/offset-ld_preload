#include "stopcomm.h"
#include <time.h>
#include <stdio.h>
#include <sys/select.h>
#include <stdbool.h>

bool pselect_sleep(const struct timespec *timeout)
{
	fd_set readfds, writefds, exceptfds;
	FD_ZERO(&readfds);
	FD_ZERO(&writefds);
	FD_ZERO(&exceptfds);
	// timeout should be copied to the stack, and therefore not modified in the calling function
	return pselect(0, &readfds, &writefds, &exceptfds, timeout, NULL)==0;
}

bool run_test()
{
	struct stopcomm_request request = {
		{0,  500000000},
		{7, 0}
	};
	stopcomm_requests(1, &request);
	struct timespec start;
	if (clock_gettime(CLOCK_REALTIME, &start)!=0) {
		perror("clock_gettime (start)");
		return false;
	}
	struct timespec req = {
		2,
		500000000
	};
	if (!pselect_sleep(&req)) {
		perror("pselect()");
		return false;
	}
	struct timespec end;
	if (clock_gettime(CLOCK_REALTIME, &end)!=0) {
		perror("clock_gettime (end)");
		return false;
	}
	if (end.tv_sec-start.tv_sec<2 || end.tv_sec-start.tv_sec>3 ||
	    (end.tv_sec-start.tv_sec)*1000000000+(end.tv_nsec-start.tv_nsec)<2500000000 ||
	    (end.tv_sec-start.tv_sec)*1000000000+(end.tv_nsec-start.tv_nsec)>2505000000
	) {
		printf("pselect() sleep (single stop) test FAILED: clock_gettime(REALTIME) measures a run time of %ld seconds and %ld nanoseconds\n", end.tv_sec-start.tv_sec, end.tv_nsec-start.tv_nsec);
		return false;
	}
	return true;
}
