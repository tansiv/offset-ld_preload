#define _GNU_SOURCE
#include "stopcomm.h"
#include <time.h>
#include <sys/time.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>

bool run_test()
{
	struct timeval start;
	if (gettimeofday(&start, NULL)!=0) {
		perror("gettimeofday (start)");
		return false;
	}
	if (usleep(2*1000000+500000)!=0) {
		perror("usleep didn’t sleep for the requested time");
		return false;
	}
	struct timeval end;
	if (gettimeofday(&end, NULL)!=0) {
		perror("gettimeofday (end)");
		return false;
	}
	if (end.tv_sec-start.tv_sec<2 ||
	    end.tv_sec-start.tv_sec>3 ||
	    (end.tv_sec-start.tv_sec-2)*1000000+(end.tv_usec-start.tv_usec)<500000+0 ||
	    (end.tv_sec-start.tv_sec-2)*1000000+(end.tv_usec-start.tv_usec)>500000+1000
	) {
		printf("usleep() (no stop) test FAILED: gettimeofday() measures a run time of %ld seconds and %ld microseconds\n", end.tv_sec-start.tv_sec, end.tv_usec-start.tv_usec);
		return false;
	}
	return true;
}
